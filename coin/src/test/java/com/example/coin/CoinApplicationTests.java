package com.example.coin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.example.coin.Controller.CoinController;
import com.example.coin.Dao.CoinRepository;
import com.example.coin.Model.Coin;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(CoinController.class)
class CoinApplicationTests {
	@Autowired
    MockMvc mockMvc;
    @Autowired
    ObjectMapper mapper;
	
    @MockBean
	CoinRepository coinRepository;

	Coin coin1 = new Coin("USD", "美金", "46,666.4983");
	Coin coin2 = new Coin("GBP", "英鎊", "46,666.4983");
	Coin coin3 = new Coin("EUR", "歐元", "42,494.4200");
	List<Coin> coins = new ArrayList<>(Arrays.asList(coin1, coin2, coin3));


	@Test
	void contextLoads() {
	}


	@Test
	void CoinsTest() throws Exception {
		Mockito.when(coinRepository.findAll()).thenReturn(coins);
		System.out.println(
		mockMvc.perform(MockMvcRequestBuilders
		.get("/api/coins")
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.jsonPath("$[1].code", Matchers.is("GBP")))
		.andReturn().getResponse().getContentAsString());
		
	}

	@Test
	void getOneCoinTest() throws Exception {
		Mockito.when(coinRepository.findByCode(coin1.getCode())).thenReturn(java.util.Optional.of(coin1));
		System.out.println(
		mockMvc.perform(MockMvcRequestBuilders
		.get("/api/coin/USD")
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.jsonPath("$.code", Matchers.notNullValue()))
		.andExpect(MockMvcResultMatchers.jsonPath("$.code", Matchers.is("USD")))
		.andReturn().getResponse().getContentAsString());
		
		
	}

	@Test
	void putCoinsTest() throws Exception {
		Coin coinNTD = new Coin("NTD", "台幣", "999,999.0000");
		Mockito.when(coinRepository.save(coinNTD)).thenReturn(coinNTD);
		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/api/updateCoin")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(coinNTD));
	}	

	@Test
	public void updateCoinTest() throws Exception {
		Coin updatedCoin = new Coin("USD", "台幣", "999,999.0000");

		Mockito.when(coinRepository.findById(coin1.getMid())).thenReturn(Optional.of(coin1));
		Mockito.when(coinRepository.save(updatedCoin)).thenReturn(updatedCoin);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/updateCoin")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.content(this.mapper.writeValueAsString(updatedCoin));
	}

	@Test
	public void deletePatientById_success() throws Exception {
		Mockito.when(coinRepository.findById(coin2.getMid())).thenReturn(Optional.of(coin2));

		mockMvc.perform(MockMvcRequestBuilders
		.delete("/api/deleteCoin/2")
		.contentType(MediaType.APPLICATION_JSON));
	}

	@Test
	public void getJson() throws Exception {
		System.out.println(

		mockMvc.perform(MockMvcRequestBuilders
		.get("/api/getCoinDesk"))
		.andReturn().getResponse().getContentAsString());

	}

	@Test
	public void formatJson() throws Exception {
		System.out.println(
		mockMvc.perform(MockMvcRequestBuilders
		.get("/api/putCoinDesk"))
		.andReturn().getResponse().getContentAsString());

	}
}
