package com.example.coin.Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import com.example.coin.Dao.CoinRepository;
import com.example.coin.Model.Coin;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import Service.CoinService;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

@RestController
@RequestMapping("/api")
public class CoinController {

    @Autowired
    private CoinRepository coinRepository;
    
    @Autowired
    private CoinService coinService;

    @GetMapping("/coins")
    public Collection<Coin> coins() {
        return coinRepository.findAll();
    }

    @GetMapping("/coin/{code}")
    public ResponseEntity<Coin> getCoin(@PathVariable String code) {
        Optional<Coin> coin = coinRepository.findByCode(code);
        return coin.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/createCoin")
    public Collection<Coin>  createCoin(@RequestBody Coin coin) throws Exception {
        Timestamp ts = new Timestamp(System.currentTimeMillis());  
        String tsStr = "";  
        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        tsStr = sdf.format(ts);
        coin.setUpdateTime(tsStr);
        coinRepository.save(coin);
        return coinRepository.findAll();

    }

    @PutMapping("/updateCoin")
    public ResponseEntity<Coin> updateCoin(@RequestBody Coin coin) {
        Coin rtncoin = coinRepository.findByCode(coin.getCode()).get();
        rtncoin.setCode_CH(coin.getCode_CH());
        Timestamp ts = new Timestamp(System.currentTimeMillis());  
        String tsStr = "";  
        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        tsStr = sdf.format(ts);
        rtncoin.setUpdateTime(tsStr);
        Coin result = coinRepository.save(rtncoin);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/deleteCoin/{id}")
    public List<Coin> deleteCoin(@PathVariable Long id){
        coinRepository.deleteById(id);
        return coinRepository.findAll();
    }

    @GetMapping("getCoinDesk")
    public JSONObject readJsonFromUrl() throws Exception {
        String url = "https://api.coindesk.com/v1/bpi/currentprice.json";
        InputStream is = new URL(url).openStream();
        try {
          BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
          String jsonText = readAll(rd);
          JSONObject json = JSONObject.fromObject(jsonText);
          return (JSONObject) json;
        } finally {
          is.close();
        }
    }

    private JSONObject readJsonFromUrlApi() throws Exception {
        String url = "https://api.coindesk.com/v1/bpi/currentprice.json";
        InputStream is = new URL(url).openStream();
        try {
          BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
          String jsonText = readAll(rd);
          JSONObject json = JSONObject.fromObject(jsonText);
          return (JSONObject) json;
        } finally {
          is.close();
        }
    }

    @GetMapping("putCoinDesk")
    public Collection<Coin>  getJsonData() throws Exception {
        JSONObject json = readJsonFromUrlApi();
        Map<String, Map<String, String>> result = new ObjectMapper().readValue(json.get("bpi").toString(), HashMap.class);
        
        Map<String, String> EUR = result.get("EUR");
        Coin CoinEUR = formatCoin(EUR);
        coinRepository.save(CoinEUR);

        Map<String, String> USD = result.get("USD");
        Coin CoinUSD = formatCoin(USD);
        coinRepository.save(CoinUSD);    

        Map<String, String> GBP = result.get("GBP");
        Coin CoinGBP = formatCoin(GBP);
        coinRepository.save(CoinGBP); 
        return coinRepository.findAll();
    }

    public Coin formatCoin(Map<String, String> coinData) {
        Timestamp ts = new Timestamp(System.currentTimeMillis());  
        String tsStr = "";  
        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        tsStr = sdf.format(ts);       
        Coin coin = new Coin();
        coin.setCode(coinData.get("code").toString());
        String code_ch = coinData.get("code").toString();
        if(code_ch.equals("USD"))
            coin.setCode_CH("美金");
        else if(code_ch.equals("GBP"))
            coin.setCode_CH("英鎊");
        else if(code_ch.equals("EUR"))
            coin.setCode_CH("歐元");
        coin.setRate(coinData.get("rate").toString());
        coin.setUpdateTime(tsStr);
        return coin;
    }

    private String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
          sb.append((char) cp);
        }
        return sb.toString();
    }
}

