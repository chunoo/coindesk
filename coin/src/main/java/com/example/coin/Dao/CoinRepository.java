package com.example.coin.Dao;

import java.sql.Timestamp;
import java.util.Optional;

import com.example.coin.Model.Coin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import lombok.NonNull;

@Repository
public interface CoinRepository extends JpaRepository<Coin, Long>{
    Optional<Coin> findByCode(String code);

}
