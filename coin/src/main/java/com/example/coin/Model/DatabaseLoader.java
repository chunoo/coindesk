package com.example.coin.Model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.coin.Dao.CoinRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLoader implements CommandLineRunner {

    @Autowired
    private CoinRepository coinRepository;

    @Override
    public void run(String... args) throws Exception {
        new Thread(){
            public void run(){
                Coin coin = new Coin("BitCoin", "比特幣", "1.0");
                Timestamp ts = new Timestamp(System.currentTimeMillis());  
                String tsStr = "";  
                DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                tsStr = sdf.format(ts);  
                coin.setUpdateTime(tsStr);
                coinRepository.save(coin);                
            }
        }.start();
    }
}
