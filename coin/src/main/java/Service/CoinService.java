package Service;

import java.sql.Timestamp;
import java.util.Optional;

import com.example.coin.Dao.CoinRepository;
import com.example.coin.Model.Coin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;


public interface CoinService {

    @Query("select * from coin where coin.code = ?1")
    Optional<Coin> findByCode(String code);

}
